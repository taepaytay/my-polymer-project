'use strict';

var ldap = require('ldapjs');
const assert = require('assert');

var searchBase = 'ou=tpbprofile,dc=ldtest,dc=com';
var cnset= 'cn=user1,';
var username1 = 'user55';

function addNode(client){
   var entry = {
      sn: 'u4',
      userPassword: '{MD5}CY9rzUYh03PK3k6DJie09g==',
      objectclass: 'person'
    };

    var base = cnset + searchBase;

    client.add(base, entry, function(err) {
      assert.ifError(err);
      console.log('add complet!')
      unbind(client);
    });  
}

function compare(client){
  var base = cnset + searchBase;
  var matched = "";
    client.compare(base, 'sn', 'u4', function(err, matched) {
      assert.ifError(err);
      console.log('matched: ' + matched);
      unbind(client);
    });
}

function delNode(client){

  var dn = cnset + searchBase;
  client.del(dn, function(err) {
    assert.ifError(err);
    process.exit(0);
  });
}

function modifNode(client){
  var base = cnset + searchBase;
  var newVal = 'cn=user4'

  client.modifyDN(base, newVal, function(err) {
      assert.ifError(err);
      console.log('modify success!')
      unbind(client);
  }); 
}

function searchNode(username, client, callback){
  var resu = false;
  var opts = {
    filter: '(cn=' + username + ')',
    scope: 'sub',
    attributes: [],
    sizeLimit: 1
  };  

  client.search(searchBase, opts, function(err, res) {
    assert.ifError(err);

    res.on('searchEntry', function(entry) {
      resu = true;
      console.log('entry: ' + JSON.stringify(entry.object));
    });
    res.on('searchReference', function(referral) {
      console.log('referral: ' + referral.uris.join());
    });
    res.on('error', function(err) {
      console.error('error: ' + err.message);
    });
    res.on('end', function(result) {
      console.log('status: ' + result.status);
    });
    // unbind(client);
    setTimeout(function(){
      callback(resu + username);
    }, 1000);
  });
}

function unbind(client){
  client.unbind(function(err) {
    assert.ifError(err);
    // process.exit(0);
  }); 
}

function bindNode(client){
    var base = cnset + searchBase;
    console.log(base);
    client.bind(base, 'test', function(err, matched) {
      // assert.ifError(err);
      console.log('matched: ' + matched);
      // unbind(client);
    });
}

function main(){
    var client = ldap.createClient({
        url: 'ldap://127.0.0.1:389'
    });

    var dn = "cn=Manager,dc=ldtest,dc=com";

    client.bind(dn, 'secret', function(err) {
      assert.ifError(err);
    });
    
    // bindNode(client)

    // addNode(client);
    // compare(client);
    // delNode(client);
    // modifNode(client);
    // searchNode(client);
    searchNode(username1, client, function(res){
      console.log(res);
      unbind(client)
      return res;
    });

    setTimeout(function(){
      unbind(client);
    }, 3000);
}

main();
