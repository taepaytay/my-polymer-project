'use strict';

const express = require('express');
var bodyParser = require('body-parser');
 
var conLdap = require('./conLDAP')
var passAtt = require('./addUserToldap')
const app = express();

app.use( bodyParser.json() );

app.use(express.static('public/lib'));
app.use(express.static('src'));

app.use(bodyParser.urlencoded({
  extended: true
}));

var port = 3000

app.get('/', function (req, res, next) {
    res.sendFile(__dirname +'/src/views/index.html')
    next()
}, function(req, res){
    console.log('This is post')
}); 

app.post('/', function(req, res){
    res.sendFile(__dirname +'/src/views/index.html')
});

app.get('/register', function (req, res, next) {
    res.sendFile(__dirname +'/src/views/reg-page.html')
    next()
}, function(req, res){
    console.log('This is reg page')
});

app.post('/register-done', function(req, res){
    // passAtt.getAtt(req.body)
    res.redirect('/register-done');
});

app.get('/register-done', function (req, res, next) {
    res.sendFile(__dirname +'/src/views/reg-done-page.html')
    next()
}, function(req, res){
    console.log('This is reg-done page')
});


app.listen(port, function () {
    console.log('App listening on port ' + port)
})

app.use(express.static('src/views'));

 
