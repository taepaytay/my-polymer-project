'use strict';

const ldap = require('ldapjs');
const assert = require('assert');

var routeset;
var URL = 'ldap://127.0.0.1:389';
var dn = "cn=Manager,dc=ldtest,dc=com";
var searchBase = 'ou=testprofile,dc=ldtest,dc=com';

exports.add = function(obj){
    console.log(obj)

    var d = new Date(obj.reg_bdate);
    // console.log(d)

    // console.log(d.getDate());
    // console.log(d.getMonth());
    // console.log(d.getFullYear());

    var myEpoch = d.getTime()/1000.0;
    console.log(myEpoch)
    // var myDate = new Date( myEpoch *1000);
 
    // console.log(myDate)

    var data = {};
    data.reg_username = obj.reg_username;
    data.reg_password = obj.reg_password;
    data.reg_nametitle = obj.reg_nametitle;
    data.reg_fname = obj.reg_fname;
    data.reg_lname = obj.reg_lname;
    data.reg_bdate = myEpoch;
    data.reg_mbnumber = obj.reg_mbnumber;
    data.reg_email = obj.reg_email;
    data.reg_org_name = obj.reg_org_name;
    data.reg_org_id = obj.reg_org_id;

    // console.log(data);

    routeset = 'uid=' + data.reg_username + ',';

    return new Promise(
        (resolve) => {
            connectLDAP(data)
            .then(addNode)
            .then(resolve); 
        }
    );
}

exports.repass = function(obj){
    // console.log(obj);
    var data = {};

    data.username = obj.uid;
    data.newpassword = obj.new_password;

    return new Promise(
        (resolve) => {
            connectLDAP(data)
            .then(repassword)
            .then(resolve); 
        }
    );
}

function connectLDAP(data){
    return new Promise(
        (resolve) => {
            var client = ldap.createClient({
                url: URL
            });

            client.bind(dn, 'secret', function(err) {
                console.log('-----bind LDAP----- con ldap')
                assert.ifError(err);
            });
            resolve([data, client])
            // resolve(true)
        }
    );
}

function addNode(args){
    var data = args[0];
    var client = args[1];

    var entry = {
        objectclass: 'person',
        objectclass: 'inetOrgPerson',

        userPassword: data.reg_password,
        title: data.reg_nametitle,

        givenName: data.reg_fname,
        sn: data.reg_lname,
        cn: data.reg_fname + ' ' + data.reg_lname,
        displayName: data.reg_fname + ' ' + data.reg_lname,
        
        mail: data.reg_email,
        telephoneNumber: data.reg_mbnumber,
        // mobile: data.reg_mbnumber,
        mobile: data.reg_bdate,

        organizationName: data.reg_org_name,
        organizationalUnitName: data.reg_org_id,
        
        description: 'unactive',
    };

    var base = routeset + searchBase;

    return new Promise(
        (resolve) => {
            client.add(base, entry, function(err) {
              assert.ifError(err);
              // console.log('add complet!')
              resolve(true)
              unbind(client);
            });  
        }
    );
}

function repassword(args){
    var client = args[1];
    var data = args[0];

    var Tpath = 'uid=' + data.username + ',' + searchBase;

    // console.log(Tpath);

    var change = new ldap.Change({
        operation: 'replace',
        modification: {
            userPassword: [data.newpassword]
        }
    });

    return new Promise(
        (resolve) => {
            client.modify(Tpath, change, function(err) {
                assert.ifError(err);
                client.unbind(function(err) {
                    console.log('-----unbind LDAP----- con ldap')
                    assert.ifError(err);
                    resolve(true);
                });
            });
        }
    );
}

function unbind(client){
  client.unbind(function(err) {
    console.log('-----unbind LDAP----- con ldap')
    assert.ifError(err);
    // process.exit(0);
  });
}

