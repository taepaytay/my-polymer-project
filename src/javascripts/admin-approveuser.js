'use strict';

const ldap = require('ldapjs');
const assert = require('assert');

var URL = 'ldap://127.0.0.1:389'
var dn = "cn=Manager,dc=ldtest,dc=com";
var searchBase = 'ou=testprofile,dc=ldtest,dc=com';

exports.approveUser = function(username){
    return new Promise(
        (resolve) => {
            connectLDAP(username)
            .then(appUser)
            .then(resolve)
        }
    );
}

exports.approveUsers = function(username){
    return new Promise(
        (resolve) => {
            // console.log(username)
            connectLDAP(username)
            .then(appUsers)
            .then(resolve);
            // resolve(true);
        }
    );
}

function connectLDAP(username){
    var client = ldap.createClient({
        url: URL
    });

    return new Promise(
        (resolve) => {

            client.bind(dn, 'secret', function(err) {
                console.log('-----bind LDAP----- (ADMIN app user)')
                assert.ifError(err);
                resolve([client, username]);
            });

        }
    );
}

function unbind(client){
  client.unbind(function(err) {
    console.log('-----unbind LDAP----- (ADMIN app user)')
    assert.ifError(err);
  });
}

function appUser(args){
    var client = args[0];
    var username = args[1];

    var Tpath = 'uid=' + username + ',' + searchBase;

    console.log('2 ' + username + ' ' + Tpath);

    var change = new ldap.Change({
        operation: 'replace',
        modification: {
            description: ['active']
        }
    });

    return new Promise(
        (resolve) => {
            client.modify(Tpath, change, function(err) {
                assert.ifError(err);
                resolve(true);
                unbind(client);
            });
        }
    );
}

function appUsers(args){
    var client = args[0];
    var accounts = args[1];
    
    var i = accounts.length-1;

    var username = accounts[i]

    // console.log(username);
    // console.log(i);
    // console.log(accounts);

    var Tpath = 'uid=' + username + ',' + searchBase;

    console.log(username + ' ' + Tpath);

    var change = new ldap.Change({
        operation: 'replace',
        modification: {
            description: ['active']
        }
    });

    return new Promise(
        (resolve) => {
            client.modify(Tpath, change, function(err) {
                assert.ifError(err);

                if(i > 0){
                    appUsersRecur(i-1, client, accounts)
                    .then(resolve);
                }else{
                    unbind(client);
                    resolve(true)   
                }
            });
        }
    );
}

function appUsersRecur(i, client, accounts){
    var username = accounts[i]

    // console.log(username);
    // console.log(i);
    // console.log(accounts);

    var Tpath = 'uid=' + username + ',' + searchBase;

    console.log(username + ' ' + Tpath);

    var change = new ldap.Change({
        operation: 'replace',
        modification: {
            description: ['active']
        }
    });
    
    return new Promise(
        (resolve) => {
            client.modify(Tpath, change, function(err) {
                assert.ifError(err);

                if(i > 0){
                    appUsersRecur(i-1, client, accounts)
                    .then(resolve);
                }else{
                    unbind(client);
                    resolve(true)   
                }
            });
        }
    );
}
