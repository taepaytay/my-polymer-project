'use strict';

const ldap = require('ldapjs');
const assert = require('assert');

var URL = 'ldap://127.0.0.1:389';
var dn = "cn=Manager,dc=ldtest,dc=com";
var searchBase = 'ou=testprofile,dc=ldtest,dc=com';

exports.editUser = function(user){
    return new Promise(
        (resolve) => {
            connectLDAP(user)
            .then(editUser)
            .then(resolve)
        } 
    );
}

function connectLDAP(user){
    return new Promise(
        (resolve) => {
            var client = ldap.createClient({
                url: URL
            });

            client.bind(dn, 'secret', function(err) {
                console.log('-----bind LDAP----- (EDIT USER)')
                assert.ifError(err);
                resolve([client, user]);
            });

        }
    );
}

function unbind(client){
    client.unbind(function(err) {
        console.log('-----unbind LDAP----- (EDIT USER)')
        assert.ifError(err);
    });
}

function editUser(args){

    var client = args[0],
    user = args[1];

    console.log(user);

    var Tpath = 'uid=' + user.edit_user_username + ',' + searchBase;

    console.log(user.edit_user_username + ' ' + Tpath);

    var d = new Date(user.edit_user_bdate);
    var myEpoch = d.getTime()/1000.0;

    var changeGN = new ldap.Change({
        operation: 'replace',
        modification: {
            givenName: [user.edit_user_fname]
        }
    });

    var changeSN = new ldap.Change({
        operation: 'replace',
        modification: {
            sn: user.edit_user_lname
        }
    });

    var changeCN = new ldap.Change({
        operation: 'replace',
        modification: {
            cn: user.edit_user_fname + ' ' + user.edit_user_lname,
        }
    });

    var changeDisplayName = new ldap.Change({
        operation: 'replace',
        modification: {
            displayName: user.edit_user_fname + ' ' + user.edit_user_lname
        }
    });

    var changeTelephoneNumber = new ldap.Change({
        operation: 'replace',
        modification: {
            telephoneNumber: user.edit_user_mobile
        }
    });

    var changeBdate = new ldap.Change({
        operation: 'replace',
        modification: {
            mobile: myEpoch
        }
    });

    return new Promise(
        (resolve) => {

            client.modify(Tpath, changeGN, function(err) {
                assert.ifError(err);
                client.modify(Tpath, changeSN, function(err) {
                    assert.ifError(err);
                    client.modify(Tpath, changeCN, function(err) {
                        assert.ifError(err);
                        client.modify(Tpath, changeDisplayName, function(err) {
                            assert.ifError(err);
                            client.modify(Tpath, changeTelephoneNumber, function(err) {
                                assert.ifError(err);
                                client.modify(Tpath, changeBdate, function(err) {
                                    assert.ifError(err);
                                    resolve(true);
                                    unbind(client);
                                });
                            });
                        });
                    });
                });
            });
        }
    );
}