'use strict';

const ldap = require('ldapjs');
const assert = require('assert');

// var f = 'objectclass=person';
var f = 'objectclass=person';
var URL = 'ldap://127.0.0.1:389';
var dn = "cn=Manager,dc=ldtest,dc=com";
var searchBase = 'ou=testprofile,dc=ldtest,dc=com';

exports.getNewuser = function(){
    f = 'description=unactive';
    return new Promise(
        (resolve) => {
            connectLDAP()
            .then(getalluser)
            .then(resolve)
        }
    );
}

exports.getCurrentuser = function(){
    f = 'description=active';
    return new Promise(
        (resolve) => {
            connectLDAP()
            .then(getalluser)
            .then(resolve)
        }
    );
}

function connectLDAP(){
    return new Promise(
        (resolve) => {
            var client = ldap.createClient({
                url: URL
            });

            client.bind(dn, 'secret', function(err) {
                // console.log('-----bind LDAP----- (ADMIN get user)')
                assert.ifError(err);
            });

            resolve(client)
        }
    );
}

function getalluser(client){
    var resu = [];
    var opts = {
        filter: f,
        scope: 'sub',
        attributes: [],
        sizeLimit: 20000
    }; 

    return new Promise(
        (resolve) => {
            client.search(searchBase, opts, function(err, res) {
                assert.ifError(err);

                res.on('searchEntry', function(entry) {
                    // console.log('entry: ' + JSON.stringify(entry.object));
                    resu.unshift(entry.object);
                    // resolve(resu)
                });

                res.on('searchReference', function(referral) {
                    console.log('referral: ' + referral.uris.join());
                });

                res.on('error', function(err) {
                    console.error('error: ' + err.message);
                });

                res.on('end', function(result) {
                    console.log('get ' + resu.length + ' accounts.');
                    // console.log('status: ' + result.status);
                    unbind(client);
                    resolve(resu)
                });
            })
        }
    );
}


function unbind(client){
  client.unbind(function(err) {
    // console.log('-----unbind LDAP----- (ADMIN get user)')
    assert.ifError(err);
  });
}


