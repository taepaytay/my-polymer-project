'use strict';

const ldap = require('ldapjs');
const assert = require('assert');

var searchBase = 'ou=testprofile,dc=ldtest,dc=com';
var dn = "cn=Manager,dc=ldtest,dc=com";
var URL = 'ldap://127.0.0.1:389';

exports.deleteUser = function(username){
    return new Promise(
        (resolve) => {
            connectLDAP(username)
            .then(delUser)
            .then(resolve)
        }
    );
}

function connectLDAP(username){
    return new Promise(
        (resolve) => {
            var client = ldap.createClient({
                url: URL
            });

            client.bind(dn, 'secret', function(err) {
                console.log('-----bind LDAP----- (ADMIN del user)')
                assert.ifError(err);
                resolve([client, username]);
            });

        }
    );
}

function unbind(client){
  client.unbind(function(err) {
    console.log('-----unbind LDAP----- (ADMIN del user)')
    assert.ifError(err);
  });
}


function delUser(args){
    return new Promise(
        (resolve) => {
            var client = args[0];
            var username = args[1];
            var Tpath = 'uid=' + username + ',' + searchBase;

            // console.log('2 ' + username + ' ' + dn);
            
            client.del(Tpath, function(err) {
                assert.ifError(err);
                resolve(true);
                unbind(client);
            })
            // resolve(false)
        }
    );
}