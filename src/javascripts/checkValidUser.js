'use strict';

const ldap = require('ldapjs');
const assert = require('assert');

var searchBase = 'ou=testprofile,dc=ldtest,dc=com';
var dn = "cn=Manager,dc=ldtest,dc=com";
var URL = 'ldap://127.0.0.1:389';

exports.check = function(username){
    // console.log(username)
    return new Promise(
        (resolve) => {
            connectLDAP(username)
            .then(searchNode)
            .then(resolve);
        }
    );
}

exports.checks = function(accounts){
    var reg_accounts = accounts
    // console.log(reg_accounts)
    return new Promise( 
        (resolve) => {
            connectLDAP(reg_accounts)
            .then(presearchNode)
            .then(resolve);
        }
    );
}

function connectLDAP(username){
    return new Promise(
        (resolve) => {
            var client = ldap.createClient({
                url: URL
            });

            client.bind(dn, 'secret', function(err) {
                console.log('-----bind LDAP----- (CHK USER)')
                assert.ifError(err);

                resolve([username, client])
                // resolve(true)
            });
        }
    );
}

function searchNode(args){
    var username = args[0];
    var client = args[1];

    var resu = false;
    var Tpath = searchBase;
    
    var opts = {
        filter: '(uid=' + username + ')',
        scope: 'sub',
        attributes: ['uid'],
        sizeLimit: 1
    }; 

    return new Promise(
        (resolve) => {

            client.search(Tpath, opts, function(err, res) {
                assert.ifError(err);

                res.on('searchEntry', function(entry) {
                    resu = true;
                    // console.log('entry: ' + JSON.stringify(entry.object));
                });

                res.on('searchReference', function(referral) {
                    console.log('referral: ' + referral.uris.join());
                });

                res.on('error', function(err) {
                    console.error('error: ' + err.message);
                });

                res.on('end', function(result) {
                    // console.log('status: ' + result.status);
                      client.unbind(function(err) {
                        console.log('-----unbind LDAP----- (CHK USER)')
                        assert.ifError(err);
                        resolve(resu);
                      });
                    // unbind(client);
                });
            });
        }
    );
}

function presearchNode(args){
    var client = args[1];
    var accounts = args[0];

    var Tpath = searchBase;
    var resu = false;

    var i = accounts.length-1;

    return new Promise(
        (resolve) => {
            var username = accounts[i];
            var opts = {
                filter: '(uid=' + username + ')',
                scope: 'sub',
                attributes: ['uid'],
                sizeLimit: 1
            };

            client.search(Tpath, opts, function(err, res) {
                assert.ifError(err);

                res.on('searchEntry', function(entry) {
                    resu = true;
                    console.log('entry: ' + JSON.stringify(entry.object));
                });

                res.on('searchReference', function(referral) {
                    console.log('referral: ' + referral.uris.join());
                });

                res.on('error', function(err) {
                    console.error('error: ' + err.message);
                });

                res.on('end', function(result) {
                    // console.log('status: ' + result.status);
                    // resolve(resu);
                    // unbind(client);
                    if(resu && i > 0){
                        // console.log(i);
                        // console.log(accounts);
                        searchNodeRecur(i-1, accounts, client)
                        .then(resolve)
                    }else{
                        unbind(client);
                        resolve(resu)
                    }
                });
            });
        }
    );
}

function searchNodeRecur(i ,accounts, client){
    return new Promise(
        (resolve) => {
            var username = accounts[i];

            var Tpath = searchBase;            
            var resu = false;

            var opts = {
                filter: '(uid=' + username + ')',
                scope: 'sub',
                attributes: ['uid'],
                sizeLimit: 1
            };

            client.search(Tpath, opts, function(err, res) {
                assert.ifError(err);

                res.on('searchEntry', function(entry) {
                    resu = true;
                    console.log('entry: ' + JSON.stringify(entry.object));
                });

                res.on('searchReference', function(referral) {
                    console.log('referral: ' + referral.uris.join());
                });

                res.on('error', function(err) {
                    console.error('error: ' + err.message);
                });

                res.on('end', function(result) {
                    // console.log('status: ' + result.status);
                    // resolve(resu);
                    // unbind(client);
                    if(resu && i > 0){
                        // console.log(i);
                        // console.log(accounts.length);
                        searchNodeRecur(i-1, accounts, client)
                        .then(resolve)
                    }else{
                        unbind(client);
                        resolve(resu);
                    }
                });
            });
        }
    );
}

function unbind(client){
  client.unbind(function(err) {
    console.log('-----unbind LDAP----- (CHK USER)')
    assert.ifError(err);
  });
}
