'use strict';

const pg = require('pg');

var username = 'postgres';
var password = 'admin';
var port = '5432'
var database = 'IAAM'

const connectionString = process.env.DATABASE_URL || 'postgres://' + username + ':' + password + '@localhost:' + port + '/' + database;

// console.log(connectionString);
exports.getOrg = function(){
    return new Promise(
        (resolve) => {
            connectDB()
            .then(query)
            .then(resolve)
        }
    );
}

function connectDB(){
    return new Promise(
        (resolve) => {
            const client = new pg.Client(connectionString);
            client.connect();
            resolve(client);
        }
    );
}

function query(client){
    return new Promise(
        (resolve) => {
            var results = []
            const query = client.query('SELECT * FROM public.organization');

            // Stream results back one row at a time
            query.on('row', (row) => {
              results.push(row);
            });

            // After all data is returned, close connection and return results
            query.on('end', () => {
                client.end();
                resolve(results);
                console.log('send : ' + results.length) 
            });
        }
    );
}