'use strict';

const ldap = require('ldapjs');
const assert = require('assert');

var cnset;
var URL = 'ldap://127.0.0.1:389';
var dn = "cn=Manager,dc=ldtest,dc=com";
var searchBase = 'ou=testprofile,dc=ldtest,dc=com';

exports.check = function(email){
    var reg_email = email

    cnset = 'cn=' + reg_email + ',';

    return new Promise(
        (resolve) => {
            connectLDAP(reg_email)
            .then(searchNode)
            .then(resolve);
        }
    );
}

function connectLDAP(email){
    return new Promise(
        (resolve) => {
            var client = ldap.createClient({
                url: URL
            });

            client.bind(dn, 'secret', function(err) {
                console.log('-----bind LDAP----- (CHK EMAIL)')
                assert.ifError(err);
                resolve([email, client]);
            });
        }
    );
}

function searchNode(agrs){
    var email = agrs[0];
    var client = agrs[1];

    var resu = false;
    
    var opts = {
        filter: '(cn=' + email + ')',
        scope: 'sub',
        attributes: ['cn'],
        sizeLimit: 1
    }; 

    return new Promise(
        (resolve) => {

            client.search(searchBase, opts, function(err, res) {
                assert.ifError(err);

                res.on('searchEntry', function(entry) {
                    resu = true;
                    // console.log('entry: ' + JSON.stringify(entry.object));
                });

                res.on('searchReference', function(referral) {
                    console.log('referral: ' + referral.uris.join());
                });

                res.on('error', function(err) {
                    console.error('error: ' + err.message);
                });

                res.on('end', function(result) {
                    // console.log('status: ' + result.status);
                    resolve(resu);
                    unbind(client);
                });
            });
        }
    );
}

function unbind(client){
  client.unbind(function(err) {
    console.log('-----unbind LDAP----- (CHK EMAIL)')
    assert.ifError(err);
  });
}
