'use strict';

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', {title: 'IAAM Authen'});
    next();
}, function(req, res){
    console.log('Index page');
});

module.exports = router;
