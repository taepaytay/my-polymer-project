'use strict';

var express = require('express');
var router = express.Router();

/* GET regis page. */
router.get('/', function(req, res, next){
    res.render('reg-page');
    next();
}, function(req, res){
    console.log('register page');
});

module.exports = router;
