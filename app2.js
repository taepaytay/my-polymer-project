'use strict';

const express = require('express');
const bodyParser = require('body-parser');

var conLdap = require('./src/javascripts/conldap');
var conPSQL = require('./src/javascripts/getOrgFromPSQL');
var chkUser = require('./src/javascripts/checkValidUser');
var chkEmail = require('./src/javascripts/checkValidEmail');

var admin = require('./src/javascripts/admin-getuser');
var delAcc = require('./src/javascripts/admin-deleteuser')
var appAcc = require('./src/javascripts/admin-approveuser')
var editAcc = require('./src/javascripts/admin-edituser')

var routes = require('./routes/index');
var registerPage = require('./routes/register')

var app = express();
var port = process.env.PORT || 3001;
var org = [];

app.use(express.static('public/lib'));
app.use(express.static('src'))
app.use(bodyParser.json() );
app.use(bodyParser.urlencoded({
  extended: true
}));

app.set('views', './src/views');
app.set('view engine', 'ejs');

app.use('/', routes);

//------------------- REGISTER PAGE------------------------
app.use('/register', registerPage);

app.post('/register-done', function(req, res, next){
    chkUser.check(req.body.reg_username)
    .then(function(chk){
        // console.log(chk)
        if(chk){
            // console.log('Invalid')
            res.redirect('/')
            // res.send('invalid username / E-mail')
        }else{
            // console.log('Valid')
            // console.log(req.body)
            conLdap.add(req.body)
            .then(function(resu){
                if(resu){
                    res.render('regis-done-page', {email: req.body.reg_email});
                }else{
                    res.redirect('/')
                }
                next();
            });
        }
    })
}, function(req, res){
    console.log('register finish');
});

app.get('/get-org', function(req, res, next){
    if (org.length == 0) {
        console.log("Query");
        conPSQL.getOrg()
        .then(function(result){
            // console.log(result);
            org = result;
            res.send(org);
        })
    } else {
        console.log("No Query");
        res.send(org);
    }
    
},function(){
    console.log('send ORG')
});

app.get('/chkUser', function(req, res){
    // console.log('get user')
    // console.log(req.query.reg_username)

    chkUser.check(req.query.reg_username)
    .then(function(chk){
        if(chk){
            // console.log('Invalid')
            res.send({status: false});
        }else{
            // console.log('Valid')
            res.send({status: true});
        }
    });
});

app.get('/chkEmail', function(req, res){
    // console.log('get email')
    // console.log(req.query.reg_email)

    chkEmail.check(req.query.reg_email)
    .then(function(chk){
        if(chk){
            // console.log('Invalid')
            res.send({status: false});
        }else{
            // console.log('Valid')
            res.send({status: true});
        }
    });
});

//-------------------------------FOR GET PASSWORD PAGE------------

app.get('/forget-pass', function(req, res, next){
    // res.send('This is forget password page');
    res.render('forgetpass-page');
    next();
}, function(req, res){
    console.log('forget password');
});

app.post('/get-pass', function(req, res, next){
    chkUser.check(req.body.username)
    .then(function(chk){
        if(chk){
            res.render('forgetpass-done-page')
        }else{
            setTimeout(function(){
                res.redirect('/forget-pass')
            }, 500)
        }
        next();
    })
}, function(req, res){
    console.log('get password done!')
});

app.get('/repassword', function(req, res, next){
    // res.send('This is forget password page');
    res.render('repassword', {uid: 'pecpec005'});
    next();
}, function(req, res){
    console.log('re password');
});

app.get('/renewPass', function(req, res, next){
    chkUser.check(req.query.uid)
    .then(function(chk){
        if(chk){
            conLdap.repass(req.query).
            then(function(resu){
                // console.log(resu);
                if(resu){
                    res.send({result: resu});
                }else{
                    res.send({result: false});
                }
            });
        }else{
            setTimeout(function(){
                res.send({result: false});
            }, 500);
        }
    })
}, function(req, res){
    // console.log('re password');
});

//------------------------ADMIN PAGE----------------------------

app.get('/admin-get-newuser', function(req, res, next){
    admin.getNewuser()
    .then(function(user){
        res.send(user)
        // console.log(user)
        next();
    })
},function(req, res){
    // console.log('send all new user')
});

app.get('/admin-get-currentuser', function(req, res, next){
    admin.getCurrentuser()
    .then(function(user){
        res.send(user)
        // console.log(user)
        next();
    })
},function(req, res){
    // console.log('send all current user')
});

app.get('/admin-page', function(req, res, next){
    res.render('admin-page')
}, function(req, res){
    console.log('ADMIN PAGE!')
});

//----------------------DELETE AND APPROVE USERACCOUNT---------------

app.get('/deleteAccount', function(req, res, next){
    // console.log('1 ' + req.query.del_username);
    chkUser.check(req.query.del_username)
    .then(function(chk){
        // console.log(chk);
        if(chk){
            delAcc.deleteUser(req.query.del_username)
            .then(function(resu){
                console.log('delete user : ' + req.query.del_username)
                res.send({name: req.query.del_username, result: resu});
            });
        }else{
            res.send({name: req.query.del_username, result: false});
        }
    });
    // next();
}, function(req, res){
    console.log('DELETE USER!')
});

app.get('/approveAccount', function(req, res, next){
    console.log('1 ' + req.query.app_username);
    chkUser.check(req.query.app_username)
    .then(function(chk){
        console.log(chk);
        if(chk){
            appAcc.approveUser(req.query.app_username)
            .then(function(resu){
                res.send({name: req.query.app_username, result: resu});
            });
        }else{
            res.send({name: req.query.app_username, result: false});
        }
    });
    next();
}, function(req, res){
    console.log('APPROVE USER!')
});

app.get('/approveAccounts', function(req, res, next){
    // var testdata = [1,2,3,4,5]
    var data = req.query.app_usernames;
    // console.log(req.query.app_usernames);
    chkUser.checks(data)
    // chkUser.checks(testdata)
    .then(function(resu){
        if(resu){
            // console.log(testdata);
            appAcc.approveUsers(data)
            .then(function(resu){
                res.send({name: req.query.app_username, result: resu});
            });
        }else{
            res.send({name: req.query.app_username, result: false});
        }
    });
    next();
}, function(req, res){
    console.log('MULTI APPROVE!')
});

app.get('/editAccount', function(req, res, next){
    var data = req.query;
    // console.log(data);

    chkUser.check(data.edit_user_username)
    .then(function(resu){
        if(resu){
            // console.log(data);
            editAcc.editUser(data)
            .then(function(resu){
                res.send({name: req.query.app_username, result: resu});
            });
        }else{
            res.send({name: req.query.app_username, result: false});
        }
    });
    next();
}, function(req, res){
    console.log('EDIT ACCOUNT')
});

app.listen(port, function () { 
    console.log('App2 listening on port ' + port)
})  